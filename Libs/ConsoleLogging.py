import logging

class PrintHandler(logging.Handler):
    """A Handler Class that logs to a PostgreSQL database"""

    def __init__(self):
        logging.Handler.__init__(self)

    def emit(self, record):
        try:
            msg = self.format(record)
            print(msg)
        except (KeyboardInterrupt, SystemExit):
            raise
        except Exception as e:
            self.handleError(record)