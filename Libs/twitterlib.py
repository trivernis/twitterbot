import twitter,logging
from Libs import datalib
logging.getLogger(__name__).addHandler(logging.NullHandler())
tweet_file = datalib.TweetFile()

class Tweet:
    def __init__(self, text, media = None, source = None, hashtag = None):
        self.text = text
        self.media = media
        self.source = source
        self.hashtag = hashtag
        self.sent = False
        self.id = None
        self.is_duplicate = self._check_duplicate()
        logging.debug('Tweet Duplicate {}'.format(self.is_duplicate))

    def _check_duplicate(self):
        return (tweet_file.contains_key_value('media', self.media) or tweet_file.contains_key_value('source', self.source) or tweet_file.contains_key_value('text', self.text))

    def get_text(self):
        ttxt = self.text
        if self.hashtag:
            ttxt+=" {}".format(self.hashtag)
        if self.source:
            ttxt+= " {}".format(self.source)
        logging.debug("Formatted Tweet with text {}".format(ttxt))
        return ttxt

    def get_dict(self):
        return self.__dict__

    def save(self):
        tweet_file.set_category(str(self.id), self.get_dict())

class TweetEngine:
    def __init__(self, api):
        self.api = api

    def send_tweet(self, tweet):
        try:
            tweet_text = tweet.get_text()

            if len(tweet_text) <= 140 and not tweet.is_duplicate and not tweet.sent:
                status = self.api.PostUpdate(tweet_text, media = tweet.media)
                tweet.id = status.id
                tweet.sent = True
                tweet.save()
                logging.info('Tweeted "{}"'.format(tweet_text))
            else:
                logging.warning('Tweet {} is longer than 140 characters'.format(tweet_text))
                tweet.save()

        except Exception as e:
            logging.exception(e)
            tweet.save()