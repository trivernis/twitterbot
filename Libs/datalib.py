import json, os, logging

logging.getLogger(__name__).addHandler(logging.NullHandler())


class DataFile():
    def __init__(self):
        self.path = './Data/coredata.json'
        self._data = self._get_json()

    def _get_json(self):
        if os.path.isfile(self.path):
            try:
                with open(self.path) as data_file:
                    logging.info('Loading json-file')
                    return json.load(data_file)
            except Exception as e:
                logging.exception(e)
                return {}
        else:
            self._set_json()
            return {}

    def _set_json(self):
        try:
            with open(self.path, 'w') as data_file:
                json.dump(self._data, data_file, sort_keys=True, indent=2)
                logging.debug('{} overwritten'.format(self.path))
        except Exception as e:
            logging.exception(e)

    def add_category(self, category_name):
        if type(category_name) is str:
            self._data[category_name] = None
            logging.info('Added category {}'.format(category_name))
            self._set_json()
            return True
        else:
            logging.error('category_name {} is not a string'.format(category_name))
            return False

    def set_category(self, category_name, value):
        if type(category_name) is str:
            self._data[category_name] = value
            logging.debug('Added {} to category {}'.format(value, category_name))
            self._set_json()
            return True
        else:
            logging.error('category_name {} is not a string'.format(category_name))
            return False

    def get_category(self, category_name):
        try:
            return self._data[category_name]
        except Exception as e:
            logging.exception(e)
            return {}


class TweetFile():
    def __init__(self):
        self.path = './Data/tweetdata.json'
        self._data = self._get_json()

    def _get_json(self):
        if os.path.isfile(self.path):
            try:
                with open(self.path) as data_file:
                    logging.info('Loading json-file')
                    return json.load(data_file)
            except Exception as e:
                logging.exception(e)
                return {}
        else:
            self._set_json()
            return {}

    def _set_json(self):
        try:
            with open(self.path, 'w') as data_file:
                json.dump(self._data, data_file, sort_keys=True, indent=2)
                logging.debug('{} overwritten'.format(self.path))
        except Exception as e:
            logging.exception(e)

    def add_category(self, category_name):
        if type(category_name) is str:
            self._data[category_name] = None
            logging.info('Added category {}'.format(category_name))
            self._set_json()
            return True
        else:
            logging.error('category_name {} is not a string'.format(category_name))
            return False

    def set_category(self, category_name, value):
        if type(category_name) is str:
            self._data[category_name] = value
            logging.debug('Added {} to category {}'.format(value, category_name))
            self._set_json()
            return True
        else:
            logging.error('category_name {} is not a string'.format(category_name))
            return False

    def get_category(self, category_name):
        try:
            return self._data[category_name]
        except Exception as e:
            logging.exception(e)
            return None

    def contains_key_value(self, key, value):
        for dkey in self._data.keys():
            if type(self._data[dkey]) is dict:
                if key in self._data[dkey].keys():
                    return self._data[dkey][key] is value
        return False

class TweetAgendaFile():
    def __init__(self):
        self.path = './Data/tweetagenda.json'
        self._data = self._get_json()

    def _get_json(self):
        if os.path.isfile(self.path):
            try:
                with open(self.path) as data_file:
                    logging.info('Loading json-file')
                    return json.load(data_file)
            except Exception as e:
                logging.exception(e)
                return {}
        else:
            self._set_json()
            return {}

    def _set_json(self):
        try:
            with open(self.path, 'w') as data_file:
                json.dump(self._data, data_file, sort_keys=True, indent=2)
                logging.debug('{} overwritten'.format(self.path))
        except Exception as e:
            logging.exception(e)

    def add_category(self, category_name):
        if type(category_name) is str:
            self._data[category_name] = None
            logging.info('Added category {}'.format(category_name))
            self._set_json()
            return True
        else:
            logging.error('category_name {} is not a string'.format(category_name))
            return False

    def set_category(self, category_name, value):
        if type(category_name) is str:
            self._data[category_name] = value
            logging.debug('Added {} to category {}'.format(value, category_name))
            self._set_json()
            return True
        else:
            logging.error('category_name {} is not a string'.format(category_name))
            return False

    def get_category(self, category_name):
        try:
            return self._data[category_name]
        except Exception as e:
            logging.exception(e)
            return None

    def get_all(self):
        try:
            return self._data
        except Exception as e:
            logging.exception(e)
            return None

    def remove_category(self, category_name):
        categories = self._data.keys()

        if category_name in categories:
            self._data = {item for item in self._data if item.key is not category_name}
            self._set_json()

    def contains_key_value(self, key, value):
        for dkey in self._data.keys():
            if type(self._data[dkey]) is dict:
                if key in self._data[dkey].keys():
                    return self._data[dkey][key] is value
        return False