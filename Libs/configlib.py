import configparser,logging
logging.getLogger(__name__).addHandler(logging.NullHandler())

class ConfigFile:
    def __init__(self):
        self.path = './Conf/settings.ini'
        self.default_path = './Conf/default_settings.ini'
        self.parser = configparser.ConfigParser()
        self.default_parser = configparser.ConfigParser()
        self.parser.read_file(open(self.path))
        self.default_parser.read_file(open(self.default_path))

    def get_section(self, section_name):
        value = None
        if type(section_name) is str:
            if section_name in self.parser.sections():
                value = dict(self.parser[section_name])
            elif section_name in self.default_parser.sections():
                value = dict(self.default_parser[section_name])
            else:
                logging.error('Section {} is not a settings section'.format(section_name))
        else:
            logging.error('Section-name {} is not a string'.format(section_name))
        return value

def convert_string_to(string, datatype):
    if string == 'None':
        return
    if datatype is str:
        return string
    elif datatype is int:
        return int(string)
    elif datatype is list:
        returnlist = []
        string = string.replace(' ', '')
        for value in string.split(','):
            returnlist.append(value)
        return returnlist
