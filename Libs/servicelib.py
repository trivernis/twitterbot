import logging, feedparser
from bs4 import BeautifulSoup
from Libs import twitterlib, datalib
import urllib.request as urlreq

logging.getLogger(__name__).addHandler(logging.NullHandler())
datafile = datalib.DataFile()

class NineGag:
    def __init__(self, sections):
        if type(sections) is list:
            self.sections = sections
        elif type(sections) is str:
            self.sections = [sections]
        else:
            if sections:
                logging.warning('*sections* not str or list')
            self.sections = []
        self._data = datafile.get_category('NineGag')
        if not self._data:
            self._data = {'posts':{}}

    def _get_posts(self, section):
        logging.debug('getting posts...')
        try:
            post_ids = []
            html = urlreq.urlopen('https://9gag.com/{}'.format(section)).read()
            soup = BeautifulSoup(html, 'html.parser')
            for post in soup.find_all('article'):
                if post['data-entry-id'] not in self._data['posts'].keys():
                    post_ids.append(post['data-entry-id'])
            return [self._get_post(id) for id in post_ids]
        except Exception as e:
            logging.exception(e)
            return False

    def _get_post(self, id):
        logging.debug('getting post-data for id {}'.format(id))
        try:
            html = urlreq.urlopen('https://9gag.com/gag/{}'.format(id)).read()
            soup = BeautifulSoup(html, 'html.parser')
            image = soup.find_all(self._is_post_image)[0]
            logging.debug('found Post {}'.format(image['src']))
            count = 0
            self._data['posts'][id]= {'title':image['alt'], 'image':image['src']}
            return twitterlib.Tweet(image['alt'], media = image['src'], source = 'https://9gag.com/gag/{}'.format(id), hashtag = '#9gag')
        except IndexError:
            pass
        except Exception as e:
            logging.exception(e)
            return

    def _is_post_image(self, tag):
        try:
            return ('badge-item-img' in tag['class'] and not 'hide' in tag['class'])  # no gifs, thats why
        except:
            return False

    def _set_data(self):
        posts = list(self._data['posts'].keys())
        if len(posts) > (len(self.sections)*10):
            _allposts = self._data['posts']
            self._data['posts'] = {}
            for id in posts[:-(len(self.sections)*10)]:
                self._data['posts'][id] = _allposts[id]
        datafile.set_category('NineGag', self._data)

    def get_tweets(self):
        tweets = []
        for section in self.sections:
            logging.debug('getting tweets for 9gag-section {}...'.format(section))
            tweets.extend([tweet for tweet in list(self._get_posts(section)) if tweet is not None])
        logging.debug(tweets)
        if len(tweets)>0:
            self._set_data()
        return tweets

class RSS:
    def __init__(self, urls):
        if type(urls) is list:
            self.urls = urls
        elif type(urls) is str:
            self.urls = [urls]
        else:
            if urls:
                logging.warning('*urls* not str or list')
            self.urls = []
        self._data = datafile.get_category('RSS')
        if not self._data:
            self._data = {'articles': []}

    def _get_latest(self, url):
        feed = feedparser.parse(url)
        if feed['bozo'] is not 1 and len(feed['items'])>0:
            article = feed['items'][0]
            if article['link'] not in self._data['articles']:
                self._data['articles'].append(article['link'])
                return twitterlib.Tweet(article['link'], hashtag = '#rss')
        return

    def _set_data(self):
        articles = self._data['articles']
        if len(articles) > (len(self.urls)*10):
            self._data['articles'] = articles[:-(len(self.urls)*10)]
        datafile.set_category('RSS', self._data)

    def get_tweets(self):
        tweets = []
        for url in self.urls:
            logging.debug('getting tweets for rss-url {}'.format(url))
            try:
                tweet = self._get_latest(url)
                if tweet:
                    tweets.append(tweet)
            except Exception as e:
                logging.exception(e)
        logging.debug(tweets)
        if len(tweets)>0:
            self._set_data()
        return tweets