#!/usr/bin/env bash
sudo apt update
sudo apt install libpq-dev python3-dev
if [hash pip3];then
    sudo pip3 install colorlog
    sudo pip3 install psycopg2
    sudo pip3 install python-twitter
    sudo pip3 install feedparser
    sudo pip3 install configparser
    sudo pip3 install bs4
    exit 0
fi
if [hash pip];then
    sudo pip install colorlog
    sudo pip install psycopg2
    sudo pip install python-twitter
    sudo pip install feedparser
    sudo pip install configparser
    sudo pip install bs4
    exit 0
fi