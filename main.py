import logging, os, configparser
from logging.config import fileConfig
import Libs.SQLLogging as sqllog
import bot


def main():
    twitterBot = bot.TwitterBot()
    twitterBot.run_loop()


if __name__ == '__main__':
    fileConfig('./Conf/logging_config.ini')
    logger = logging.getLogger()
    if os.path.isfile('./Conf/SQL_config.ini'):
        try:
            parser = configparser.ConfigParser()
            parser.read('./Conf/SQL_config.ini')
            conf = parser['SQL']
            connection =sqllog.connect(database=conf['database'],user=conf['user'], password=conf['password'],host=conf['adress'],port=int(conf['port']))
            handler = sqllog.PostgreSQLHandler(connection,tablename=conf['table'])
            handler.setLevel(logging.WARNING)
            logger.addHandler(handler)
        except Exception as e:
            logger.exception(e)
    logger.info("Created Logger")
    main()