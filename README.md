### What is this repository for? ###

This is a twitterbot that automatically tweets certain content.

### How do I get set up? ###
* Requirements:
This bot works with python 3.5 or higher. Version 3.6 is recommended.
* Setup:
Download the stable version via `git clone https://bitbucket.org/trivernis/twitterbot.git` or alternatively the developement version `git clone https://bitbucket.org/trivernis/twitterbot.git`.
* Dependencies:
Install the dependencies by executing the install.sh script via `bash install.sh`. You need sudo rights to do this.
* Database configuration:
To configure the SQL-Logging database create a file named *SQL_config.ini* in the *Conf* folder. The file template should look like this:
`[SQL]
adress=youradress
port=yourport
user=youruser
password=yourpassword
database=yourdatabase
table=yourtable
`
* How to run:
Run this bot in the command line with `sudo python main.py`