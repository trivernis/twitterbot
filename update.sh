#!/usr/bin/env bash
cd ..
if [ -d "twitterbot/" ];then
    sudo git clone https://bitbucket.org/trivernis/twitterbot.git twitterbot_update
    sudo rsync -a ./twitterbot_update/ ./twitterbot/
    sudo rm -r ./twitterbot_update/
    sudo chmod -R u+rw ./twitterbot/
    exit 0
else
    sudo git clone https://bitbucket.org/trivernis/twitterbot.git twitterbot
    sudo chmod -R u+rw ./twitterbot/
    exit 0
fi