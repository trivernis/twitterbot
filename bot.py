import twitter, asyncio, logging
from random import randint
from Libs import datalib, configlib, twitterlib, servicelib
logging.getLogger(__name__).addHandler(logging.NullHandler())

class TwitterBot:
    def __init__(self):
        self.datafile = datalib.DataFile()
        self.agendafile = datalib.TweetAgendaFile()
        self.configfile = configlib.ConfigFile()
        self.authData = self.configfile.get_section('Auth_Data')
        self.settings = self.configfile.get_section('General')
        self.api = self._get_api()
        self.user = self._verify()
        self.tweet_engine = twitterlib.TweetEngine(self.api)
        self.tweet_agenda = asyncio.Queue()
        self.event_loop = asyncio.get_event_loop()

    def __del__(self):
        del self.api

    def _get_api(self):
        try:
            _auth = self.authData
            api = twitter.Api(consumer_key=_auth['consumer_key'],
                              consumer_secret=_auth['consumer_secret'],
                              access_token_key=_auth['token_key'],
                              access_token_secret=_auth['token_secret'])
            logging.info('Authentificated.')
            return api
        except Exception as e:
            logging.exception(e)
            return None

    def _verify(self):
        try:
            if not self.api:
                self.api = self._get_api()
            user = self.api.VerifyCredentials()
            logging.info('Logged in with %s', user.name)
            return user
        except Exception as e:
            logging.exception(e)
            return None

    async def _random_pause(self):
        try:
            interval = int(self.settings['interval'])
            deviation = int(self.settings['deviation'])
            pausedur = randint(interval - deviation, interval + deviation)
            logging.debug('Pausing for {} seconds'.format(pausedur))
            await asyncio.sleep(pausedur)
        except Exception as e:
            logging.exception(e)

    async def _tweet_coroutine(self):
        """The Coroutine for tweeting. Because: Why NOT?"""
        logging.debug('In _tweet_coroutine')
        if not self.tweet_agenda.empty():
            logging.debug('Tweets in agenda. Starting tweeting.')
            item = await self.tweet_agenda.get()
            self.tweet_engine.send_tweet(item)

    async def _data_coroutine(self):
        """The Data-Coroutine. Checking for new Updates"""
        logging.debug('In _data_coroutine')
        self.ninegag = servicelib.NineGag(configlib.convert_string_to(self.configfile.get_section('Nine_Gag')['sections'], list)) #The sections are stored in the settings-file
        self.rss = servicelib.RSS(configlib.convert_string_to(self.configfile.get_section('RSS')['urls'], list))
        count = 0
        # Ninegag
        for tweet in self.ninegag.get_tweets():
            logging.debug(tweet)
            await self.tweet_agenda.put(tweet)
            count +=1
        # RSS
        for tweet in self.rss.get_tweets():
            logging.debug(tweet)
            await self.tweet_agenda.put(tweet)
            count +=1

        if count>0:
            logging.info('Added {} tweets to the agenda'.format(count))
        logging.debug('in loop for {}'.format(self.event_loop.time()))

    async def _main_coroutine(self):
        """The Main routine for the twitter bot. Everything here is looped. Wuhuuu"""
        logging.debug('In _main_coroutine')
        await self._load_agenda()
        while True:
            await self._data_coroutine()
            await self._tweet_coroutine()
            await self._random_pause()

    async def _load_agenda(self):
        tweets = self.agendafile.get_all()
        for prototweet in tweets:
            tweet = twitterlib.Tweet(prototweet['text'], media = prototweet['media'], source = prototweet['source'], hashtag = prototweet['hashtag'])
            await self.tweet_agenda.put(tweet)

    async def _save_agenda(self):
        count = 0
        while not self.tweet_agenda.empty():
            tweet = await self.tweet_agenda.get()
            self.agendafile.set_category(count, tweet.get_dict())
            count+=1

    def run_loop(self):
        try:
            logging.info('starting eventloop')
            self.event_loop.run_until_complete(self._main_coroutine())
        except KeyboardInterrupt:
            self.event_loop.stop()
        except Exception as e:
            logging.exception(e)
        finally:
            try:
                self.event_loop.run_until_complete(self._save_agenda())
            except Exception as ex:
                logging.exception(ex)
            logging.info('bot is stopping now')
            self.__del__()